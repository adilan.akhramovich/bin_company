import requests
import psycopg2
from datetime import datetime, timedelta

# Database connection details
database = "rwh_gis_datalake"
user = "rwh_analytics"
password = "4HPzQt2HyU@"
host = "172.30.227.205"
port = "5439"

# Connect to the PostgreSQL database and create the table
connection = psycopg2.connect(
    database=database,
    user=user,
    password=password,
    host=host,
    port=port
)

cursor = connection.cursor()

create_table_query = '''
CREATE TABLE IF NOT EXISTS qamqor_crimes (
    objectid BIGINT,
    id BIGINT,
    yr NUMERIC,
    period NUMERIC,
    crime_code NUMERIC,
    time_period NUMERIC,
    hard_code NUMERIC,
    ud NUMERIC,
    organ TEXT,
    dat_vozb BIGINT,
    dat_sover BIGINT,
    stat TEXT,
    descr TEXT,
    dat_vozb_str TEXT,
    dat_sover_str TEXT,
    tz1id TEXT,
    reg_code TEXT,
    city_code TEXT,
    status BIGINT,
    org_code TEXT,
    entrydate BIGINT,
    fz1r18p5 TEXT,
    fz1r18p6 TEXT,
    transgression TEXT,
    organ_kz TEXT,
    organ_en TEXT,
    fe1r29p1_id TEXT,
    fe1r32p1 TEXT,
    globalid TEXT,
    weekday INTEGER,
    x_coord FLOAT,
    y_coord FLOAT
)
'''
cursor.execute(create_table_query)
connection.commit()

# Function to insert data into the database
def insert_data_into_database(data):
    for feature in data:
        attributes = feature.get("attributes", {})
        geometry = feature.get("geometry", {})

        # Extract the values from the attributes
        objectid = attributes.get("objectid")
        id = attributes.get("id")
        yr = attributes.get("yr")
        period = attributes.get("period")
        crime_code = attributes.get("crime_code")
        time_period = attributes.get("time_period")
        hard_code = attributes.get("hard_code")
        ud = attributes.get("ud")
        organ = attributes.get("organ")
        dat_vozb = attributes.get("dat_vozb")
        dat_sover = attributes.get("dat_sover")
        stat = attributes.get("stat")
        descr = attributes.get("descr")
        dat_vozb_str = attributes.get("dat_vozb_str")
        dat_sover_str = attributes.get("dat_sover_str")
        tz1id = attributes.get("tz1id")
        reg_code = attributes.get("reg_code")
        city_code = attributes.get("city_code")
        status = attributes.get("status")
        org_code = attributes.get("org_code")
        entrydate = attributes.get("entrydate")
        fz1r18p5 = attributes.get("fz1r18p5")
        fz1r18p6 = attributes.get("fz1r18p6")
        transgression = attributes.get("transgression")
        organ_kz = attributes.get("organ_kz")
        organ_en = attributes.get("organ_en")
        fe1r29p1_id = attributes.get("fe1r29p1_id")
        fe1r32p1 = attributes.get("fe1r32p1")
        globalid = attributes.get("globalid")
        weekday = attributes.get("weekday")
        x = geometry.get("x")
        y = geometry.get("y")

        # Construct the INSERT query
        insert_query = """
            INSERT INTO qamqor_crimes (objectid, id, yr, period, crime_code, time_period, hard_code, ud, organ, dat_vozb, 
            dat_sover, stat, descr, dat_vozb_str, dat_sover_str, tz1id, reg_code, city_code, status, org_code, entrydate, 
            fz1r18p5, fz1r18p6, transgression, organ_kz, organ_en, fe1r29p1_id, fe1r32p1, globalid, weekday, x_coord, y_coord)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
            %s, %s, %s, %s, %s, %s, %s);
        """

        # Execute the query with the extracted values
        cursor.execute(insert_query, (
            objectid, id, yr, period, crime_code, time_period, hard_code, ud, organ, dat_vozb, dat_sover,
            stat, descr, dat_vozb_str, dat_sover_str, tz1id, reg_code, city_code, status, org_code, entrydate,
            fz1r18p5, fz1r18p6, transgression, organ_kz, organ_en, fe1r29p1_id, fe1r32p1, globalid, weekday,
            x, y))

    connection.commit()

# Function to calculate the date range from 01-01-2023 to the current date
def calculate_date_range():
    start_date = datetime(2023, 1, 1)
    end_date = datetime(2023, 1, 22)  # Current date
    return start_date, end_date

# Function to get crime data with the specified query
def get_crime_data(query):
    url = "https://gis.kgp.kz/server/rest/services/Hosted/crime_point2022_/FeatureServer/0/query"
    params = {
        "f": "json",
        "returnIdsOnly": "false",
        "where": query,
        "returnGeometry": "true",
        "spatialRel": "esriSpatialRelIntersects",
        "outSR": "4326",
        "outFields": "*"
    }

    try:
        print(f"Executing query: {url} - {params}")
        response = requests.get(url, params=params)
        if response.status_code == 200:
            data = response.json()
            retrieved_data = data.get("features", [])
            print(f"Retrieved {len(retrieved_data)} records.")
            return retrieved_data
        else:
            print(f"Failed to retrieve data. Status code: {response.status_code}")
            print(f"Response content: {response.content}")
            return None
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while retrieving data: {e}")
        return None

# Calculate the date range
start_date, end_date = calculate_date_range()

while start_date <= end_date:
    # Define the query with the current date
    query = f"(dat_sover >= timestamp '{start_date.strftime('%Y-%m-%d')} 00:00:00') AND (dat_sover <= timestamp '{(start_date + timedelta(days=1)).strftime('%Y-%m-%d')} 00:00:00') AND (CITY_CODE='1975')"

    # Get crime data for the specified date
    crime_data = get_crime_data(query)

    if crime_data:
        insert_data_into_database(crime_data)
        print(f"Data inserted into the database for date: {start_date.strftime('%Y-%m-%d')}")

    # Increment the date by one day
    start_date += timedelta(days=1)

# Close the cursor and connection
cursor.close()
connection.close()
