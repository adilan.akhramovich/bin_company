import psycopg2
import requests
from shapely.geometry import Point
from shapely.wkt import dumps
from datetime import datetime

# Define the PostgreSQL database connection parameters
db_params = {
    'host': '172.30.227.205',
    'port': '5439',
    'database': 'rwh_gis_datalake',
    'user': 'rwh_analytics',
    'password': '4HPzQt2HyU@'
}

# Define the SQL statement to create the table with a geometry(Point) column
create_table_sql = """
CREATE TABLE IF NOT EXISTS qamqor_crime_data_2 (
    objectid serial PRIMARY KEY,
    id TEXT,
    yr TEXT,
    period TEXT,
    crime_code TEXT,
    time_period TEXT,
    hard_code TEXT,
    ud TEXT,
    organ TEXT,
    dat_vozb TEXT,
    dat_sover TEXT,
    dat_vozb_str DATE,
    dat_sover_str TIMESTAMP,
    stat TEXT,
    descr TEXT,
    tz1id TEXT,
    reg_code TEXT,
    city_code TEXT,
    status TEXT,
    org_code TEXT,
    entrydate TEXT,
    fz1r18p5 TEXT,
    fz1r18p6 TEXT,
    transgression TEXT,
    organ_kz TEXT,
    organ_en TEXT,
    fe1r29p1_id TEXT,
    fe1r32p1 TEXT,
    globalid TEXT,
    weekday TEXT,
    discontinued TEXT,
    geom geometry(Point),
    x_coord TEXT,
    y_coord TEXT
);
"""

# Define the API URL to fetch the data
url = "https://gis.kgp.kz/server/rest/services/Hosted/crime_point2022_/FeatureServer/0/query?f=json&returnIdsOnly=false&where=(dat_sover%3E=timestamp%20%272023-09-18%2000:00:00%27)%20AND%20(dat_sover%3C=timestamp%20%272023-11-07%2000:00:00%27)%20AND%20(CITY_CODE=%271975%27)&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outSR=4326&outFields=*"

# Fetch data from the API
response = requests.get(url)

if response.status_code == 200:
    data = response.json()
    records = data.get("features")
    # Print the structure of the fetched data
    print("Fetched data structure:")
    print(data)
else:
    print(f"Failed to retrieve data. Status code: {response.status_code}")
    exit()

# Establish a database connection and create a table
try:
    conn = psycopg2.connect(**db_params)
    cursor = conn.cursor()
    
    # Create the table if it doesn't exist
    cursor.execute(create_table_sql)
    conn.commit()

    # Insert the data into the table
    for record in records:
        attributes = record.get("attributes")
        geometry = record.get("geometry")
        
        # Extract the values from the attributes
        objectid = attributes.get("objectid")
        id = attributes.get("id")
        yr = attributes.get("yr")
        period = attributes.get("period")
        crime_code = attributes.get("crime_code")
        time_period = attributes.get("time_period")
        hard_code = attributes.get("hard_code")
        ud = attributes.get("ud")
        organ = attributes.get("organ")
        
        # Convert dat_vozb_str and dat_sover_str to DATE and TIMESTAMP
        dat_vozb_str = attributes.get("dat_vozb_str")
        dat_sover_str = attributes.get("dat_sover_str")
        
        dat_vozb = datetime.strptime(dat_vozb_str, "%d.%m.%Y").date() if dat_vozb_str else None
        dat_sover = datetime.strptime(dat_sover_str, "%d.%m.%Y %H:%M") if dat_sover_str else None
        
        stat = attributes.get("stat")
        descr = attributes.get("descr")
        tz1id = attributes.get("tz1id")
        reg_code = attributes.get("reg_code")
        city_code = attributes.get("city_code")
        status = attributes.get("status")
        org_code = attributes.get("org_code")
        entrydate = attributes.get("entrydate")
        fz1r18p5 = attributes.get("fz1r18p5")
        fz1r18p6 = attributes.get("fz1r18p6")
        transgression = attributes.get("transgression")
        organ_kz = attributes.get("organ_kz")
        organ_en = attributes.get("organ_en")
        fe1r29p1_id = attributes.get("fe1r29p1_id")
        fe1r32p1 = attributes.get("fe1r32p1")
        globalid = attributes.get("globalid")
        weekday = attributes.get("weekday")

        # Extract the x and y values from the geometry
        x = float(geometry.get("x", 0.0))  # Use a default value to avoid KeyError
        y = float(geometry.get("y", 0.0))
        
        # Create a Shapely Point geometry
        point_geometry = Point(x, y)
        
        # Convert the Point geometry to WKT format
        wkt_geometry = dumps(point_geometry)
        
        # Add the WKT geometry to the attributes dictionary
        attributes["geom"] = wkt_geometry
        attributes["x_coord"] = str(x)
        attributes["y_coord"] = str(y)
        attributes["dat_vozb_str"] = dat_vozb_str
        attributes["dat_sover_str"] = dat_sover_str
        
        # Modify the insert_sql statement to convert date strings to timestamps and handle None values
        insert_sql = """
        INSERT INTO qamqor_crime_data_2 (
            id, yr, period, crime_code, time_period, hard_code, ud, organ, dat_vozb, dat_sover, stat,
            descr, dat_vozb_str, dat_sover_str, tz1id, reg_code, city_code, status, org_code, entrydate,
            fz1r18p5, fz1r18p6, transgression, organ_kz, organ_en, fe1r29p1_id, fe1r32p1, globalid, weekday,
            discontinued, geom, x_coord, y_coord
        ) VALUES (
            %(id)s, %(yr)s, %(period)s, %(crime_code)s, %(time_period)s, %(hard_code)s, %(ud)s,
            %(organ)s, %(dat_vozb)s, %(dat_sover)s, %(stat)s, %(descr)s,
            NULLIF(TO_TIMESTAMP(%(dat_vozb_str)s, 'DD.MM.YYYY'), '1900-01-01'),  -- Convert to timestamp with 'DD.MM.YYYY' format
            NULLIF(TO_TIMESTAMP(%(dat_sover_str)s, 'DD.MM.YYYY HH24:MI'), '1900-01-01'),  -- Convert to timestamp with 'DD.MM.YYYY HH24:MI' format
            %(tz1id)s, %(reg_code)s, %(city_code)s, %(status)s, %(org_code)s,
            %(entrydate)s, %(fz1r18p5)s, %(fz1r18p6)s, %(transgression)s, %(organ_kz)s,
            %(organ_en)s, %(fe1r29p1_id)s, %(fe1r32p1)s, %(globalid)s, %(weekday)s, %(discontinued)s,
            ST_GeomFromText(%(geom)s, 4326), %(x_coord)s, %(y_coord)s
        );
        """

        cursor.execute(insert_sql, attributes)

    conn.commit()
    print("Data inserted successfully.")

except Exception as e:
    print(f"Error: {e}")
finally:
    cursor.close()
    conn.close()
