import requests

url = "https://gis.kgp.kz/server/rest/services/Hosted/crime_point2022_/FeatureServer/0/query?f=json&returnIdsOnly=false&where=(dat_sover%3E=timestamp%20%272023-01-01%2000:00:00%27)%20AND%20(dat_sover%3C=timestamp%20%272023-01-29%2000:00:00%27)%20AND%20(CITY_CODE=%271975%27)&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outSR=4326&outFields=*"

response = requests.get(url)

if response.status_code == 200:
    data = response.json()
    print(data)
else:
    print(f"Failed to retrieve data. Status code: {response.status_code}")
    print(f"Response content: {response.content}")
