import requests
import psycopg2
import schedule
import time
from datetime import datetime, timedelta

# Database connection details
database = "rwh_gis_datalake"
user = "rwh_analytics"
password = "4HPzQt2HyU@"
host = "172.30.227.205"
port = "5439"

# Connect to the PostgreSQL database and create the table
connection = psycopg2.connect(
    database=database,
    user=user,
    password=password,
    host=host,
    port=port
)

cursor = connection.cursor()

create_table_query = '''
CREATE TABLE IF NOT EXISTS crime_data_table_2 (
    objectid BIGINT,
    id BIGINT,
    yr NUMERIC,
    period NUMERIC,
    crime_code NUMERIC,
    time_period NUMERIC,
    hard_code NUMERIC,
    ud NUMERIC,
    organ TEXT,
    dat_vozb BIGINT,
    dat_sover BIGINT,
    stat TEXT,
    descr TEXT,
    dat_vozb_str TEXT,
    dat_sover_str TEXT,
    tz1id TEXT,
    reg_code TEXT,
    city_code TEXT,
    status BIGINT,
    org_code TEXT,
    entrydate BIGINT,
    fz1r18p5 TEXT,
    fz1r18p6 TEXT,
    transgression TEXT,
    organ_kz TEXT,
    organ_en TEXT,
    fe1r29p1_id TEXT,
    fe1r32p1 TEXT,
    globalid TEXT,
    weekday INTEGER,
    x_coord FLOAT,
    y_coord FLOAT
)
'''
cursor.execute(create_table_query)
connection.commit()

# Close the cursor and connection
cursor.close()
connection.close()

# Function to calculate the date range for the next 25 days
def calculate_date_range():
    today = datetime.now()
    start_date = today + timedelta(days=1)  # Start from tomorrow
    end_date = today + timedelta(days=25)   # End after 25 days
    return start_date.strftime('%Y-%m-%d 00:00:00'), end_date.strftime('%Y-%m-%d 00:00:00')

# Function to get crime data with the specified query
def get_crime_data(query):
    url = "https://gis.kgp.kz/server/rest/services/Hosted/crime_point2022_/FeatureServer/0/query"
    params = {
        "f": "json",
        "returnIdsOnly": "false",
        "where": query,
        "returnGeometry": "true",
        "spatialRel": "esriSpatialRelIntersects",
        "outSR": "4326",
        "outFields": "*"
    }

    try:
        print(f"Executing query: {url} - {params}")
        response = requests.get(url, params=params)
        if response.status_code == 200:
            data = response.json()
            return data.get("features", [])
        else:
            print(f"Failed to retrieve data. Status code: {response.status_code}")
            print(f"Response content: {response.content}")
            return None
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while retrieving data: {e}")
        return None



# Function to insert data into the database
def insert_data_into_database(data):
    conn = psycopg2.connect(
        database=database,
        user=user,
        password=password,
        host=host,
        port=port
    )

    cursor = conn.cursor()

    for feature in data:
        # Check if the objectid already exists in the database
        objectid = feature.get("attributes", {}).get("objectid")
        if objectid is not None:
            cursor.execute("SELECT objectid FROM crime_data_table_2 WHERE objectid = %s", (objectid,))
            existing_data = cursor.fetchone()
            if existing_data:
                print(f"Data with objectid {objectid} already exists in the table. Skipping insertion.")
                continue

        attributes = feature.get("attributes", {})
        geometry = feature.get("geometry", {})
        
        # Extract the values from the attributes
        objectid = attributes.get("objectid")
        id = attributes.get("id")
        yr = attributes.get("yr")
        period = attributes.get("period")
        crime_code = attributes.get("crime_code")
        time_period = attributes.get("time_period")
        hard_code = attributes.get("hard_code")
        ud = attributes.get("ud")
        organ = attributes.get("organ")
        dat_vozb = attributes.get("dat_vozb")
        dat_sover = attributes.get("dat_sover")
        stat = attributes.get("stat")
        descr = attributes.get("descr")
        dat_vozb_str = attributes.get("dat_vozb_str")
        dat_sover_str = attributes.get("dat_sover_str")
        tz1id = attributes.get("tz1id")
        reg_code = attributes.get("reg_code")
        city_code = attributes.get("city_code")
        status = attributes.get("status")
        org_code = attributes.get("org_code")
        entrydate = attributes.get("entrydate")
        fz1r18p5 = attributes.get("fz1r18p5")
        fz1r18p6 = attributes.get("fz1r18p6")
        transgression = attributes.get("transgression")
        organ_kz = attributes.get("organ_kz")
        organ_en = attributes.get("organ_en")
        fe1r29p1_id = attributes.get("fe1r29p1_id")
        fe1r32p1 = attributes.get("fe1r32p1")
        globalid = attributes.get("globalid")
        weekday = attributes.get("weekday")
        x = geometry.get("x")
        y = geometry.get("y")

        # Construct the INSERT query
        insert_query = """
            INSERT INTO crime_data_table_2 (objectid, id, yr, period, crime_code, time_period, hard_code, ud, organ, dat_vozb, 
            dat_sover, stat, descr, dat_vozb_str, dat_sover_str, tz1id, reg_code, city_code, status, org_code, entrydate, 
            fz1r18p5, fz1r18p6, transgression, organ_kz, organ_en, fe1r29p1_id, fe1r32p1, globalid, weekday, x_coord, y_coord)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 
            %s, %s, %s, %s, %s, %s, %s);
        """

        # Execute the query with the extracted values
        cursor.execute(insert_query, (objectid, id, yr, period, crime_code, time_period, hard_code, ud, organ, dat_vozb, 
                                      dat_sover, stat, descr, dat_vozb_str, dat_sover_str, tz1id, reg_code, city_code, 
                                      status, org_code, entrydate, fz1r18p5, fz1r18p6, transgression, organ_kz, organ_en, 
                                      fe1r29p1_id, fe1r32p1, globalid, weekday, x, y))

    try:
        conn.commit()
    except psycopg2.Error as e:
        print(f"An error occurred while inserting data into the database: {e}")

# Function to run the crime data insertion
def run_parse():
    global current_query
    start_date, end_date = calculate_date_range()
    query = f"(dat_sover >= timestamp '{start_date}') AND (dat_sover <= timestamp '{end_date}') AND (CITY_CODE='1975')"
    
    # Store the current query in the global variable
    current_query = query
    
    crime_data = get_crime_data(query)

    if crime_data:
        insert_data_into_database(crime_data)
        print("Data inserted into the database.")
    else:
        print("Data retrieval failed.")

# Function to schedule the parse
def schedule_parse():
    global current_query
    today = datetime.now()
    start_date = datetime(today.year, 1, 1)
    
    # Check if there is data from January 1, 2023, until today
    check_query = f"(dat_sover >= timestamp '{start_date}') AND (dat_sover <= timestamp '{today.strftime('%Y-%m-%d 00:00:00')}') AND (CITY_CODE='1975')"
    existing_data = get_crime_data(check_query)

    if existing_data:
        # Data exists, schedule the next run every 25 days from the current date
        print(f"Data exists from {start_date} to {today}. Scheduling next parse every 25 days from the current date.")
        schedule.every(25).days.at("00:00:00").do(run_parse)
    else:
        # Data does not exist, initiate parsing from January 1, 2023, and then every 25 days
        print(f"No data found from {start_date} to {today}. Initiating parsing sequence from {start_date}.")
        query = f"(dat_sover >= timestamp '{start_date}') AND (dat_sover <= timestamp '{start_date + timedelta(days=25)}') AND (CITY_CODE='1975')"
        
        # Store the current query in the global variable
        current_query = query
        
        # Schedule the first run with an initial delay
        schedule.every(25).days.at("00:00:00").do(run_parse)

    while True:
        time_left = schedule.idle_seconds()
        if time_left is not None:
            remaining_time = timedelta(seconds=time_left)
            print(f"Time left until next parse: {remaining_time}")
            time.sleep(60)  # Wait for a minute before checking again
        schedule.run_pending()

if __name__ == "__main__":
    schedule_parse()



if __name__ == "__main__":
    schedule_parse()