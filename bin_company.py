import requests
import xml.etree.ElementTree as ET
import time
import psycopg2
from datetime import datetime, timedelta
from tqdm import tqdm  # Import tqdm for the progress bar

# Add the access token as a query parameter
access_token = "d68bfb4e06904ca09679a69ab8acf76b"
params = {"token": access_token}

# Database configuration
db_params = {
    "host": "172.30.227.205",
    "port": "5439",
    "database": "sitcenter_postgis_datalake",
    "user": "la_noche_estrellada",
    "password": "Cfq,thNb13@"
}

create_table_query = """
CREATE TABLE IF NOT EXISTS business_company_taxes (
    bin TEXT,
    tax_org_code TEXT,
    kbk TEXT,
    entry_type TEXT,
    receipt_date TEXT,
    write_off_date TEXT,
    summa TEXT,
    pay_status TEXT,
    name_ru TEXT,
    name_kz TEXT,
    name_tax_ru TEXT,
    name_tax_kz TEXT,
    kbk_name_ru TEXT,
    kbk_name_kz TEXT,
    pay_num TEXT,
    pay_type TEXT
);
"""

def create_table_if_not_exists():
    try:
        connection = psycopg2.connect(**db_params)
        cursor = connection.cursor()
        cursor.execute(create_table_query)
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print(f"Error creating table: {error}")
    finally:
        if connection:
            cursor.close()
            connection.close()

def insert_data_to_db(bin, payment_data):
    try:
        connection = psycopg2.connect(**db_params)
        cursor = connection.cursor()

        insert_query = """
        INSERT INTO business_company_taxes (
            bin, tax_org_code, kbk, entry_type, receipt_date, write_off_date,
            summa, pay_status, name_ru, name_kz, name_tax_ru, name_tax_kz,
            kbk_name_ru, kbk_name_kz, pay_num, pay_type
        ) VALUES (
            %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
        )
        """
        data_tuple = (
            bin,
            payment_data['TaxOrgCode'],
            payment_data['KBK'],
            payment_data['EntryType'],
            payment_data['ReceiptDate'],
            payment_data['WriteOffDate'],
            payment_data['Summa'],
            payment_data['PayStatus'],
            payment_data['NameRu'],
            payment_data['NameKz'],
            payment_data['NameTaxRu'],
            payment_data['NameTaxKz'],
            payment_data['KBKNameRu'],
            payment_data['KBKNameKz'],
            payment_data['PayNum'],
            payment_data['PayType']
        )
        cursor.execute(insert_query, data_tuple)
        # print(f"Inserted data for BIN: {bin}")

        connection.commit()
    except (Exception, psycopg2.Error) as error:
        print(f"Error inserting data to DB: {error}")
    finally:
        if connection:
            cursor.close()
            connection.close()

def get_text(element, tag):
    sub_element = element.find(tag)
    if sub_element is not None:
        return sub_element.text
    return ''

create_table_if_not_exists()

while True:
    try:
        conn = psycopg2.connect(**db_params)
        cursor = conn.cursor()

        query = "SELECT bin_code, name_ru, name_kz FROM bins_company_almaty ORDER BY bin_code;"
        cursor.execute(query)
        bin_rows = cursor.fetchall()

        cursor.close()
        conn.close()

        
        # Wrap the bin_rows loop with tqdm for the progress bar
        for bin_row in tqdm(bin_rows, desc="Processing BIN"):
            bin_value, name_ru, name_kz = bin_row  # Unpack values from the query result

            xml_request = f"""
            <sign:Request xmlns:sign="http://xmlns.kztc-cits/sign" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <sign:IIN_BIN>{bin_value}</sign:IIN_BIN>
                <sign:BeginDate>2018-01-01</sign:BeginDate>
                <sign:EndDate>2022-12-31</sign:EndDate>
            </sign:Request>
            """
            
            url = "https://sb.egov.kz/proxy2/culs_payments"
            response = requests.post(url, data=xml_request, params=params)

            if response.status_code == 200:
                response_xml = response.text
                root = ET.fromstring(response_xml)
                payment_elements = root.findall(".//{http://xmlns.kztc-cits/sign}payment")

                for payment_element in payment_elements:
                    payment_data = {
                        'TaxOrgCode': get_text(payment_element, "{http://xmlns.kztc-cits/sign}TaxOrgCode"),
                        'KBK': get_text(payment_element, "{http://xmlns.kztc-cits/sign}KBK"),
                        'EntryType': get_text(payment_element, "{http://xmlns.kztc-cits/sign}EntryType"),
                        'ReceiptDate': get_text(payment_element, "{http://xmlns.kztc-cits/sign}ReceiptDate"),
                        'WriteOffDate': get_text(payment_element, "{http://xmlns.kztc-cits/sign}WriteOffDate"),
                        'Summa': get_text(payment_element, "{http://xmlns.kztc-cits/sign}Summa"),
                        'PayStatus': get_text(payment_element, "{http://xmlns.kztc-cits/sign}PayStatus"),
                        'NameRu': name_ru,  # Use the retrieved "name_ru" value
                        'NameKz': name_kz,  # Use the retrieved "name_kz" value
                        'NameTaxRu': get_text(payment_element, "{http://xmlns.kztc-cits/sign}NameTaxRu"),
                        'NameTaxKz': get_text(payment_element, "{http://xmlns.kztc-cits/sign}NameTaxKz"),
                        'KBKNameRu': get_text(payment_element, "{http://xmlns.kztc-cits/sign}KBKNameRu"),
                        'KBKNameKz': get_text(payment_element, "{http://xmlns.kztc-cits/sign}KBKNameKz"),
                        'PayNum': get_text(payment_element, "{http://xmlns.kztc-cits/sign}PayNum"),
                        'PayType': get_text(payment_element, "{http://xmlns.kztc-cits/sign}PayType")
                    }
                    insert_data_to_db(bin_value, payment_data)
            else:
                print(f"Request failed with status code: {response.status_code}")

            next_parse_time = datetime.now() + timedelta(seconds=1)
            print(f"BIN {bin_value} was parsed, next parse will start after 7 seconds. Current time: {datetime.now()}.")
            time.sleep(7)

    except Exception as e:
        print(f"An error occurred: {e}")
        time.sleep(7)
